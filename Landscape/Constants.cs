﻿namespace FxPro.JobApplication.Landscape
{
    /// <summary>
    /// Constant values used by Assembly
    /// </summary>
    internal static class Constants
    {
        /// <summary>
        /// Max Landscape length (int[] array length)
        /// </summary>
        public const int MaxLength = 32000;
        /// <summary>
        /// Max height inside Landscape array
        /// </summary>
        public const int MaxHeight = 32000;
    }
}