﻿using System;

namespace FxPro.JobApplication.Landscape
{
    public class Problem
    {
        /// <summary>
        /// Calculate the amount of water collected inside landscape pits described by <see cref="heights"/>.
        /// When the rain falls, the landscape is filled with water which is collected inside pits only. 
        /// </summary>
        /// <param name="heights">Landscape heights array</param>
        /// <returns>Amount of collected water in landscape pits</returns>
        /// <remarks>Maximum landscape length is limited with <see cref="Constants.MaxLength"/></remarks>
        /// <remarks>Maximum Hill height should non-negative and is limited with <see cref="Constants.MaxHeight"/></remarks>
        /// <exception cref="ArgumentNullException">If <see cref="heights"/> is null</exception>
        /// <exception cref="ArgumentException">If <see cref="heights"/> length exceeds <see cref="Constants.MaxLength"/>
        /// or at least one of array items breaks out its limitations: [0..<see cref="Constants.MaxHeight"/>]</exception>
        public int Solve(int[] heights)
        {
            ValidateArguments(heights);
            var length = heights.Length;
            if (length <= 2) // There's no way to save water
                return 0;

            ValidateHeight(0, heights[0]);
            ValidateHeight(length - 1, heights[length - 1]);

            int leftCursor = 0;
            int rightCursor = length - 1;
            int leftHillTop = 0;
            int rightHillTop = 0;
            var result = 0;

            // Let's move from Borders to center finding Hilltop pairs and collecting
            // water between them
            while (leftCursor < rightCursor)
            {
                // Updating left Hilltop
                if (heights[leftCursor] > leftHillTop)
                    leftHillTop = heights[leftCursor];

                // Updating right Hilltop
                if (heights[rightCursor] > rightHillTop)
                    rightHillTop = heights[rightCursor];

                // Collecting water (from lower Hilltop side)
                // Moving Cursor from Lower Hilltop side
                // and validating heights under Cursors
                if (leftHillTop >= rightHillTop)
                {
                    result += rightHillTop - heights[rightCursor--];
                    ValidateHeight(rightCursor, heights[rightCursor]);
                }
                else
                {
                    result += leftHillTop - heights[leftCursor++];
                    ValidateHeight(leftCursor, heights[leftCursor]);
                }
            }

            return result;
        }

        private void ValidateArguments(int[] heights)
        {
            if (heights == null)
                throw new ArgumentNullException(nameof(heights));
            if (heights.Length > Constants.MaxLength)
                throw new ArgumentException(
                    $"{nameof(heights)}.Length exceeds maximum value ({Constants.MaxLength})");

            // There should be validation of individual heights (minimax of 0..MaxHeight) but it costs time.
            // If we prefer fail-fast, let's check here. If we prefer to speed up, let's check inside Solve()

            /*for (int i = 0; i < heights.Length; i++)
                ValidateHeight(i, heights[i]);*/
        }

        private void ValidateHeight(int position, int height)
        {
            if (height < 0 || height > Constants.MaxHeight)
                throw new ArgumentException($"Height = {height} at position = {position} breaks out the limits");
        }
    }
}