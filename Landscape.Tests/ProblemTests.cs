﻿using System;
using System.Collections;
using NUnit.Framework;

namespace FxPro.JobApplication.Landscape.Tests
{
    [TestFixture]
    public class ProblemTests
    {
        private readonly Problem _problem;

        public ProblemTests()
        {
            _problem = new Problem();
        }

        [Test, TestCaseSource(typeof(ProblemTestCases), nameof(ProblemTestCases.TestCases))]
        public int SolveCaseTest(int[] argument)

        {
            return _problem.Solve(argument);
        }

        [Test]
        public void ArgumentNullTest()
        {
            Assert.That(() => _problem.Solve(null),
                Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void BreakingOutHeightTest()
        {
            Assert.That(() => _problem.Solve(new[] {3, -1, 4}),
                Throws.TypeOf<ArgumentException>());
            Assert.That(() => _problem.Solve(new[] {-3, 11, 4}),
                Throws.TypeOf<ArgumentException>());
            Assert.That(() => _problem.Solve(new[] {3, 1, 0, -4}),
                Throws.TypeOf<ArgumentException>());
            Assert.That(() => _problem.Solve(ProblemTestCases.MakeTooLongLandscape()),
                Throws.TypeOf<ArgumentException>());
        }
        
        [Test]
        public void TooLongLandscapeTest()
        {            
            Assert.That(() => _problem.Solve(ProblemTestCases.VeryHighHillCase),
                Throws.TypeOf<ArgumentException>());
        }
    }

    /// <summary>
    /// Test Cases Generator for ProblemTests class
    /// </summary>
    public class ProblemTestCases
    {
        public static readonly int[] VeryHighHillCase = {1, 2, 3, 2, Constants.MaxHeight + 1, 2, 4, 3, 2, 1};
        
        public static int[] MakeTooLongLandscape()
        {
            var result = new int[Constants.MaxLength + 1];
            result[0] = 1;
            result[result.Length - 1] = 1;
            return result;
        }
        
        static readonly int[] EmptyCase = { };
        static readonly int[] OneBlockCase = {1};
        static readonly int[] TwoBlocksCase = {1, 2};
        static readonly int[] ThreeBlocksCase = {3, 1, 4};
        static readonly int[] NoWaterCase = {1, 2, 3, 4, 5, 4, 4, 3, 2, 1};
        static readonly int[] SimpleCase = {1, 2, 3, 2, 5, 2, 4, 3, 2, 1};
        static readonly int[] ScreenshotCase = {5, 2, 3, 4, 5, 4, 1, 3, 1};
        static readonly int[] HugeCase = MakeHugeCase();

        private const int HugeCaseMinBoundary = 100;
        private const int HugeCaseMaxBoundary = 115;

        private static int[] MakeHugeCase()
        {
            var result = new int[Constants.MaxLength];
            result[0] = HugeCaseMinBoundary;
            result[result.Length - 1] = HugeCaseMaxBoundary;
            return result;
        }

        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData(EmptyCase)
                    .SetName(nameof(EmptyCase))
                    .Returns(0);
                yield return new TestCaseData(OneBlockCase)
                    .SetName(nameof(OneBlockCase))
                    .Returns(0);
                yield return new TestCaseData(TwoBlocksCase)
                    .SetName(nameof(TwoBlocksCase))
                    .Returns(0);
                yield return new TestCaseData(ThreeBlocksCase)
                    .SetName(nameof(ThreeBlocksCase))
                    .Returns(2);
                yield return new TestCaseData(NoWaterCase)
                    .SetName(nameof(NoWaterCase))
                    .Returns(0);
                yield return new TestCaseData(SimpleCase)
                    .SetName(nameof(SimpleCase))
                    .Returns(3);
                yield return new TestCaseData(ScreenshotCase)
                    .SetName(nameof(ScreenshotCase))
                    .Returns(8);
                yield return new TestCaseData(HugeCase)
                    .SetName(nameof(HugeCase))
                    .Returns(HugeCaseMinBoundary * (Constants.MaxLength - 2));
            }
        }

    }
}